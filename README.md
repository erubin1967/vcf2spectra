# VCF2SPECTRA

Create a template file for mass spectra analysis from VCF.

To run, download VCF2SPECTRA.v1.3.pl and the config file, and run using 

perl VCF2SPECTRA.v1.3.pl input_file_name <PathToConfigFile>


Note you might need to edit the first line of the script to point to your local perl installation