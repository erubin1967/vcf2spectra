#!/gpfs0/biores/apps/Perl/ActivePerl-5.24/bin/perl

use File::Fetch;
use LWP::Simple;
use strict;

my $expension_len= 16;

# version 1.3: Removed input file name from config file, and moved it to be the first parameter in the command line

my $eutils_base = 'https://eutils.ncbi.nlm.nih.gov/entrez/eutils';
# Check the Arguments
if ($#ARGV < 2 ) {
	print "Usage: $0 input_file_name <ConfigFile>\n";
	exit;
}

my $input_file = shift @ARGV;
my $config_file= shift @ARGV;
my $output_file= shift @ARGV;
my %Config = ();

# Source Env Variables

parse_config_file ($config_file, \%Config);  # Call the Function to create the Config hash

# Print the key value pair from the Config Hash
for my $key ( keys %Config ) {
	my $value = $Config{$key};
}

my $pep_file = $Config{"peptides_fa"};
my $column_with_mut = $Config{"column_with_mut"};
my $column_with_iden = $Config{"column_with_iden"};
my $column_with_gene_name = $Config{"column_with_gene_name"};

open(IN,"< $pep_file") || die("ERROR: Could not open pep file $pep_file to read; aborting ");

open(OUT,"> $output_file") || die("ERROR: could not open output file $output_file to write; aborting ");
open(OUT2,"> $output_file.short") || die("ERROR: could not open output file $output_file.short to write; aborting ");
print OUT2 join("\t",'ID','TranscriptID','Tumor peptide','Normal peptide'),"\n";

my (%pep_seq,%transcript2gene,$name,$seq);
while ($_=<IN>) {
	chomp;
	if (/^>(\S+).*transcript:(\S+)/) {
		$transcript2gene{$2}{$1}=1;
		$pep_seq{$name}=$seq if (defined $seq);
		$name=$1;
		$seq="";
	} else {
		$seq.=$_;
	}

}
$pep_seq{$name}=$seq if (defined $seq);
warn "INFO: Read ",scalar keys %pep_seq," protein sequences\n";
my $count=1.0;
# my $filename = $input_file;

open APRI2_in, '<', $input_file or die "Can't fetch file $input_file; error $!";

my $count_diff_cons=0;
my $count_all_snps=0;
my %done_that;
while( <APRI2_in> ) {
	chomp;
	my $snp_line = $_;
	if($_=~/missense_variant/){
		my @columns = split /\|/, $_;
		my $i=0;
		$i++ until ($columns[$i]=~/^missense/);
		for (; $i<@columns; $i+=15) {
			if ($columns[$i] eq 'missense_variant')  {
				++$count_all_snps;
				my $mut=$columns[$i+9];
				if (! $mut=~/^p\./) {
					die("Not potein in mut $mut?\n");
				}
				$mut=~s/^p.//;
				my $org=&aa_long2short(substr($mut,0,3));
				my $pos=substr($mut,3,length($mut)-6);
				my $rep=&aa_long2short(substr($mut,-3));
				my $loc = $columns[$i+5];
				my @pep_id = keys %{$transcript2gene{$loc}};
				if (@pep_id > 1) { # more than 1 transcript per peptide
					die;
				}
				my $cur = substr($pep_seq{$pep_id[0]},$pos-1,1);
				my $cur_up='';
				my $cur_down='';
				if ($pos>$expension_len) {
					$cur_up = substr($pep_seq{$pep_id[0]},$pos-int($expension_len/2)-1,int($expension_len/2));
				} else {
					$cur_up = substr($pep_seq{$pep_id[0]},0,$pos) if ($pos>0);
				}
				if ($pos+int($expension_len/2)<=length($pep_seq{$pep_id[0]})) {
					$cur_down = substr($pep_seq{$pep_id[0]},$pos,int($expension_len/2));
				} else {
					$cur_down = substr($pep_seq{$pep_id[0]},$pos-1,length($pep_seq{$pep_id[0]})-$pos+1) if ($pos<length($pep_seq{$pep_id[0]})-$pos+1);
				}
				if ($cur ne $org) { # not the same sequence?
					++$count_diff_cons;
				} else {
					my $new_seq = $pep_seq{$pep_id[0]};
					my $old_seq = $new_seq;
					substr($new_seq,$pos-1,1)=lc($rep);
					if (! exists $done_that{$pep_id[0]}) {
						print OUT ">$pep_id[0]\n";
						print OUT $old_seq,"\n";
					}
					print OUT '>'.$pep_id[0].'.'.$pos.uc($org).">".uc($rep)."\t$loc\t".$snp_line."\n";
					print OUT $new_seq,"\n";
					print OUT2 join("\t",$pep_id[0].'.'.$pos.uc($org).'>'.uc($rep),$loc,$cur_up.$rep.$cur_down,$cur_up.$cur.$cur_down),"\n";
					$done_that{$pep_id[0]}=1;
				}
				
			}
		}

	}

}


warn("WARNNING: $count_diff_cons (of $count_all_snps)  sequences not reported due to sequence inconsistancies\n");

#EXTRACT SEQUENCE FROM FASTA FILE


sub extractseq
	{
	my (@fastafile)=@_;
	use strict;
	my $sequence=' ';
	foreach my $line(@fastafile) {
		if($line=~/^\s*$/) {
			next;
		} elsif($line=~/^\s*#/) {
			next;
		} elsif($line=~/^>/) {
			next;
		} else {
			$sequence.=$line;
		}
	}
	$sequence=~s/\s//g ;
	return $sequence;
}


###################################

sub dna2peptide{
	my($dna)=@_;
	my $protein= '';

	for(my $i=0;$i<(length($dna)-2);$i+=3) {
		$protein .=codon2aa(substr($dna, $i, 3));
	}
	return $protein;
}
###################################



###############################################
sub aa_long2short {
	my $long = shift @_;
	my $long = lc($long);

	my ( %long2short ) = (
		'ala'=>'A',
		'arg'=>'R',
		'asn'=>'N',
		'asp'=>'D',
		'asx'=>'B',
		'cys'=>'C',
		'glu'=>'E',
		'gln'=>'Q',
		'glx'=>'Z',
		'gly'=>'G',
		'his'=>'H',
		'ile'=>'I',
		'leu'=>'L',
		'lys'=>'K',
		'met'=>'M',
		'phe'=>'F',
		'pro'=>'P',
		'ser'=>'S',
		'thr'=>'T',
		'trp'=>'W',
		'tyr'=>'Y',
		'val'=>'V'
	);
	if(exists $long2short{$long}) {
		return $long2short{$long};
	}
}

sub codon2aa{
	my($codon)=@_;
	$codon=uc $codon;
	my(%genetic_code)=(
		'TCA'=>'S',
		'TCC'=>'S',
		'TCG'=>'S',
		'TCT'=>'S',
		'TTC'=>'F',
		'TTT'=>'F',
		'TTA'=>'L',
		'TTG'=>'L',
		'TAC'=>'Y',
		'TAT'=>'Y',             
		#'TAA'=>'_',               
		#'TAG'=>'_',                 
		'TGC'=>'C',                   
		'TGT'=>'C',                     
		#'TGA'=>'_',                       
		'TGG'=>'W',                        
		'CTA'=>'L',                          
		'CTC'=>'L',                           
		'CTG'=>'L',                               
		'CTT'=>'L',                                 
		'CCA'=>'P',                                 
		'CCC'=>'P',                                   
		'CCG'=>'P',
		'CCT'=>'P',
		'CAC'=>'H',                                       
		'CAT'=>'H',                                          
		'CAA'=>'Q',                                             
		'CAG'=>'Q',
		'CGA'=>'R',
		'CGC'=>'R',
		'CGG'=>'R',
		'CGT'=>'R',
		'ATA'=>'I',
		'ATC'=>'I',
		'ATT'=>'I',
		'ATG'=>'M',
		'ACA'=>'T',
		'ACC'=>'T',
		'ACG'=>'T',
		'ACT'=>'T',
		'AAC'=>'N',
		'AAT'=>'N',
		'AAA'=>'K',
		'AAG'=>'K',
		'AGC'=>'S',
		'AGT'=>'S',
		'AGA'=>'R',
		'AGG'=>'R',
		'GTA'=>'V',
		'GTC'=>'V',
		'GTG'=>'V',
		'GTT'=>'V',
		'GCA'=>'A',
		'GCC'=>'A',
		'GCG'=>'A',
		'GCT'=>'A',
		'GAC'=>'D',
		'GAT'=>'D',
		'GAA'=>'E',
		'GAG'=>'E',
		'GGA'=>'G',
		'GGC'=>'G',
		'GGG'=>'G',
		'GGT'=>'G'
	);
	if(exists $genetic_code{$codon}) {
		return $genetic_code{$codon};
	} else {
		# print STDERR"bad \"$codon\"!!\n";

	}
}


# Function to Parse the Environment Variables
sub parse_config_file {
	my ($File, $Config) = @_;
	open (CONFIG, "$File") or die "ERROR: Config file not found : $File";
	while (<CONFIG>) {
		my $config_line=$_;
		chop ($config_line);          # Remove trailling \n
		$config_line =~ s/\s*//g;     # Remove spaces at the start of the line
		$config_line =~ s/\s*$//;     # Remove spaces at the end of the line
		if ( ($config_line !~ /^#/) && ($config_line ne "") ){    # Ignore lines starting with # and blank lines
			my ($key, $value) = split (/=/, $config_line);
			$$Config{$key} = $value;
		}
	}
	close(CONFIG);
}

